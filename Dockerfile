FROM python:3.6.8-jessie

WORKDIR /app

ADD setup.py .
ADD docker-entrypoint.sh .
ADD src .

ENV FLASK_APP=src/app.py

ADD requirements.txt .
RUN useradd app
RUN pip install -r requirements.txt

CMD /bin/sh docker-entrypoint.sh
